-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server versie:                5.7.19-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Versie:              9.4.0.5174
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Databasestructuur van platenbaas wordt geschreven
DROP DATABASE IF EXISTS `platenbaas`;
CREATE DATABASE IF NOT EXISTS `platenbaas` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `platenbaas`;

-- Structuur van  tabel platenbaas.authorities wordt geschreven
DROP TABLE IF EXISTS `authorities`;
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(256) DEFAULT NULL,
  `authority` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.authorities: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;

-- Structuur van  tabel platenbaas.oauth_access_token wordt geschreven
DROP TABLE IF EXISTS `oauth_access_token`;
CREATE TABLE IF NOT EXISTS `oauth_access_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` varbinary(1030) DEFAULT NULL,
  `authentication_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `authentication` varbinary(1030) DEFAULT NULL,
  `refresh_token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.oauth_access_token: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `oauth_access_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_access_token` ENABLE KEYS */;

-- Structuur van  tabel platenbaas.oauth_client_details wordt geschreven
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE IF NOT EXISTS `oauth_client_details` (
  `client_id` varchar(255) NOT NULL,
  `resource_ids` varchar(255) DEFAULT NULL,
  `client_secret` varchar(255) DEFAULT NULL,
  `scope` varchar(255) DEFAULT NULL,
  `authorized_grant_types` varchar(255) DEFAULT NULL,
  `web_server_redirect_uri` varchar(255) DEFAULT NULL,
  `authorities` varchar(255) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.oauth_client_details: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;

-- Structuur van  tabel platenbaas.oauth_client_token wordt geschreven
DROP TABLE IF EXISTS `oauth_client_token`;
CREATE TABLE IF NOT EXISTS `oauth_client_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` varbinary(1030) DEFAULT NULL,
  `authentication_id` varchar(255) NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.oauth_client_token: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `oauth_client_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_token` ENABLE KEYS */;

-- Structuur van  tabel platenbaas.oauth_code wordt geschreven
DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE IF NOT EXISTS `oauth_code` (
  `code` varchar(255) DEFAULT NULL,
  `authentication` varbinary(1030) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.oauth_code: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `oauth_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_code` ENABLE KEYS */;

-- Structuur van  tabel platenbaas.oauth_refresh_token wordt geschreven
DROP TABLE IF EXISTS `oauth_refresh_token`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_token` (
  `token_id` varchar(255) DEFAULT NULL,
  `token` varbinary(1030) DEFAULT NULL,
  `authentication` varbinary(1030) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.oauth_refresh_token: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `oauth_refresh_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_token` ENABLE KEYS */;

-- Structuur van  tabel platenbaas.order wordt geschreven
DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `orderid` int(11) NOT NULL,
  `timestamp` datetime DEFAULT NULL,
  `orderstate` varchar(50) DEFAULT NULL,
  `ordertype` varchar(50) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phonenumber` int(11) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `postalcode` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`orderid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.order: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Structuur van  tabel platenbaas.product wordt geschreven
DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `eancode` varchar(50) NOT NULL,
  `quantity` int(10) unsigned DEFAULT NULL,
  `label` varchar(50) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `artist` varchar(50) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `units` int(11) DEFAULT NULL,
  `media` varchar(50) DEFAULT NULL,
  `prefix` varchar(50) DEFAULT NULL,
  `suffix` varchar(50) DEFAULT NULL,
  `genre` varchar(50) DEFAULT NULL,
  `release` datetime DEFAULT NULL,
  PRIMARY KEY (`eancode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.product: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` (`eancode`, `quantity`, `label`, `price`, `artist`, `title`, `units`, `media`, `prefix`, `suffix`, `genre`, `release`) VALUES
	('test', 1, 'test', 1, 'test', 'test', 1, 'tes', NULL, NULL, 'test', '2017-10-12 10:40:33'),
	('test2', 2, 'test2', 2, 'test2', 'test2', 2, 'tes2', '2', '2', 'test2', '2017-10-12 10:40:33');
/*!40000 ALTER TABLE `product` ENABLE KEYS */;

-- Structuur van  tabel platenbaas.users wordt geschreven
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumpen data van tabel platenbaas.users: ~0 rows (ongeveer)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
