package nl.scalda.platenbaas.API.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author hunteroooox
 */
@Entity
@Table(name = "product")
public class Product implements Serializable {

    @Id
    @Column(name = "eancode")
    private String eancode;
    @Column(name = "quantity")
    private int quantity;
    @Column(name = "price")
    private double price;
    @Column(name = "artist")
    private String artist;
    @Column(name = "title")
    private String title;
    @Column(name = "units")
    private int units;
    @Column(name = "media")
    private String media;
    @Column(name = "label")
    private String label;
    @Column(name = "prefix")
    private String prefix;
    @Column(name = "suffix")
    private String suffix;
    @Column(name = "genre")
    private String genre;
    @Column(name = "releasedate")
    private Date release;

    public Product() {
    }

    public Product(int quantity, String eancode) {
        this.quantity = quantity;
        this.eancode = eancode;
    }

    public Product(String eancode, int quantity, double price, String artist, String title, int units, String media, String label, String prefix, String suffix, String genre, Date release) {
        
        this.eancode = eancode;
        this.quantity = quantity;
        this.price = price;
        this.artist = artist;
        this.title = title;
        this.units = units;
        this.media = media;
        this.label = label;
        this.prefix = prefix;
        this.suffix = suffix;
        this.genre = genre;
        this.release = release;
    }

    public String getEancode() {
        return eancode;
    }

    public Product setEancode(String eancode) {
        this.eancode = eancode;
        return this;
    }

    public int getQuantity() {
        return quantity;
    }

    public Product setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public double getPrice() {
        return price;
    }

    public Product setPrice(double price) {
        this.price = price;
        return this;
    }

    public String getArtist() {
        return artist;
    }

    public Product setArtist(String artist) {
        this.artist = artist;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Product setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getUnits() {
        return units;
    }

    public Product setUnits(int units) {
        this.units = units;
        return this;
    }

    public String getMedia() {
        return media;
    }

    public Product setMedia(String media) {
        this.media = media;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public Product setLabel(String label) {
        this.label = label;
        return this;
    }

    public String getPrefix() {
        return prefix;
    }

    public Product setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }

    public String getSuffix() {
        return suffix;
    }

    public Product setSuffix(String suffix) {
        this.suffix = suffix;
        return this;
    }

    public String getGenre() {
        return genre;
    }

    public Product setGenre(String genre) {
        this.genre = genre;
        return this;
    }

    public Date getRelease() {
        return release;
    }

    public Product setRelease(Date release) {
        this.release = release;
        return this;
    }

}
