package nl.scalda.platenbaas.API.model;

import java.sql.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Giovanni
 */
@Entity
@Table(name = "order")
public class Order {

    @Id
    @Column(name = "orderid")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer orderID;
    @Column(name = "timestamp")
    public Date timestamp;
    @Column(name = "orderstate")
    public String orderState;
    @Column(name = "ordertype")
    public String orderType;
    @Column(name = "total")
    public Integer totalPrice;
    @Column(name = "firstname")
    public String firstname;
    @Column(name = "prefix")
    public String prefix;
    @Column(name = "surname")
    public String surname;
    @Column(name = "email")
    public String email;
    @Column(name = "phonenumber")
    public Integer phonenumber;
    @Column(name = "country")
    public String country;
    @Column(name = "city")
    public String city;
    @Column(name = "address")
    public String address;
    @Column(name = "postalcode")
    public String postalCode;
    
    public Order(){}

    public Order(Date timestamp, String orderState, String orderType, Integer totalPrice, String firstName, String prefix, String surName, String email, Integer phoneNumber, String country, String city, String address, String postalCode) {
        this.timestamp = timestamp;
        this.orderState = orderState;
        this.orderType = orderType;
        this.totalPrice = totalPrice;
        this.firstname = firstName;
        this.prefix = prefix;
        this.surname = surName;
        this.email = email;
        this.phonenumber = phoneNumber;
        this.country = country;
        this.city = city;
        this.address = address;
        this.postalCode = postalCode;
    }

    public Integer getOrderID() {
        return orderID;
    }

    public Order setOrderID(Integer orderID) {
        this.orderID = orderID;
        return this;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public Order setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getOrderState() {
        return orderState;
    }

    public Order setOrderState(String orderState) {
        this.orderState = orderState;
        return this;
    }

    public String getOrderType() {
        return orderType;
    }

    public Order setOrderType(String orderType) {
        this.orderType = orderType;
        return this;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public Order setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public String getFirstname() {
        return firstname;
    }

    public Order setFirstName(String firstName) {
        this.firstname = firstName;
        return this;
    }

    public String getPrefix() {
        return prefix;
    }

    public Order setPrefix(String prefix) {
        this.prefix = prefix;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Order setSurName(String surName) {
        this.surname = surName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Order setEmail(String email) {
        this.email = email;
        return this;
    }

    public Integer getPhonenumber() {
        return phonenumber;
    }

    public Order setPhoneNumber(Integer phoneNumber) {
        this.phonenumber = phoneNumber;
        return this;
    }

    public String getCountry() {
        return country;
    }

    public Order setCountry(String country) {
        this.country = country;
        return this;
    }

    public String getCity() {
        return city;
    }

    public Order setCity(String city) {
        this.city = city;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public Order setAddress(String address) {
        this.address = address;
        return this;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public Order setPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

}
