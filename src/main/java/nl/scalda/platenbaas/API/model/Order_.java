package nl.scalda.platenbaas.API.model;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * @author Jeroen
 */
@StaticMetamodel(Order.class)
public class Order_ {
    /**
     * The constant orderID.
     */
    public static volatile SingularAttribute<Order, Integer> orderID;
    /**
     * The constant orderState.
     */
    public static volatile SingularAttribute<Order, String> orderState;
    /**
     * The constant orderType.
     */
    public static volatile SingularAttribute<Order, String> orderType;
    /**
     * The constant firstname.
     */
    public static volatile SingularAttribute<Order, String> firstname;
    /**
     * The constant surname.
     */
    public static volatile SingularAttribute<Order, String> surname;
    /**
     * The constant prefix.
     */
    public static volatile SingularAttribute<Order, String> prefix;
    /**
     * The constant email.
     */
    public static volatile SingularAttribute<Order, String> email;
    /**
     * The constant country.
     */
    public static volatile SingularAttribute<Order, String> country;
    /**
     * The constant city.
     */
    public static volatile SingularAttribute<Order, String> city;
    /**
     * The constant address.
     */
    public static volatile SingularAttribute<Order, String> address;
    /**
     * The constant postalCode.
     */
    public static volatile SingularAttribute<Order, String> postalCode;
    /**
     * The constant phonenumber.
     */
    public static volatile SingularAttribute<Order, Integer> phonenumber;
    /**
     * The constant totalPrice.
     */
    public static volatile SingularAttribute<Order, Integer> totalPrice;
    /**
     * The constant timestamp.
     */
    public static volatile SingularAttribute<Order, Date> timestamp;
    
}
