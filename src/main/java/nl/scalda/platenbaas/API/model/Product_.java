package nl.scalda.platenbaas.API.model;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * @author Jeroen
 */
@StaticMetamodel(Product.class)
public class Product_ {
    /**
     * The constant eancode.
     */
    public static volatile SingularAttribute<Product, String> eancode;
    /**
     * The constant quantity.
     */
    public static volatile SingularAttribute<Product, Integer> quantity;
    /**
     * The constant price.
     */
    public static volatile SingularAttribute<Product, Double> price;
    /**
     * The constant artist.
     */
    public static volatile SingularAttribute<Product, String> artist;
    /**
     * The constant title.
     */
    public static volatile SingularAttribute<Product, String> title;
    /**
     * The constant units.
     */
    public static volatile SingularAttribute<Product, Integer> units;
    /**
     * The constant media.
     */
    public static volatile SingularAttribute<Product, String> media;
    /**
     * The constant label.
     */
    public static volatile SingularAttribute<Product, String> label;
    /**
     * The constant prefix.
     */
    public static volatile SingularAttribute<Product, String> prefix;
    /**
     * The constant suffix.
     */
    public static volatile SingularAttribute<Product, String> suffix;
    /**
     * The constant genre.
     */
    public static volatile SingularAttribute<Product, String> genre;
    /**
     * The constant release.
     */
    public static volatile SingularAttribute<Product, Date> release;

}
