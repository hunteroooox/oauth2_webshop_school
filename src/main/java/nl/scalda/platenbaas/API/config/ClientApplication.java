/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.scalda.platenbaas.API.config;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.AccessTokenProviderChain;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.ClientTokenServices;
import org.springframework.security.oauth2.client.token.JdbcClientTokenServices;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeAccessTokenProvider;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author hunteroooox
 */
//@Configuration
@EnableAutoConfiguration
@EnableOAuth2Client
@SpringBootApplication
@ComponentScan({
    "nl.scalda.platenbaas.API.controllers",
    "nl.scalda.platenbaas.API.dao",
    "nl.scalda.platenbaas.API.service",
    "nl.scalda.platenbaas.API.model",
    "nl.scalda.platenbaas.API.data"})
@EntityScan({"nl.scalda.platenbaas.API.model", "nl.scalda.platenbaas.API.data"})
@EnableJpaRepositories(basePackages = {"nl.scalda.platenbaas.API.data"})
@PropertySource("classpath:application.properties")
public class ClientApplication {

    public static void main(String[] args) {
        //new SpringApplicationBuilder().sources(ClientApplication.class).run(args);
        new SpringApplicationBuilder().profiles("client").sources(ClientApplication.class).run(args);
//        SpringApplication.run(ClientApplication.class, args);
    }

    @Value("${oauth.resource}")
    private String baseUrl;

    @Value("${oauth.authorize}")
    private String authorizeUrl;

    @Value("${oauth.token}")
    private String tokenUrl;

    private AccessTokenRequest accessTokenRequest;

    @Autowired
    private DataSource dataSource;

    @RequestMapping("/")
    public List<Map<String, ?>> home() {
        List<Map<String, ?>> result = restTemplate().getForObject(baseUrl + "/admin/beans", List.class);
        return result;
    }

    @Bean
    @Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
    public OAuth2RestOperations restTemplate() {
        OAuth2RestTemplate template = new OAuth2RestTemplate(resource(), new DefaultOAuth2ClientContext(accessTokenRequest));
        AccessTokenProviderChain provider = new AccessTokenProviderChain(Arrays.asList(new AuthorizationCodeAccessTokenProvider()));
        provider.setClientTokenServices(clientTokenServices());
        return template;
    }

    @Bean
    public ClientTokenServices clientTokenServices() {
        return new JdbcClientTokenServices(dataSource);
    }

    @Bean
    protected OAuth2ProtectedResourceDetails resource() {
        AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();
        resource.setAccessTokenUri(tokenUrl);
        resource.setUserAuthorizationUri(authorizeUrl);
        resource.setClientId("webshop");

        return resource;
    }

    @Configuration
    @EnableAuthorizationServer
    public class OAuth2Config extends AuthorizationServerConfigurerAdapter {

        @Autowired
        DataSource dataSource;

        @Autowired
        private UserDetailsService userDetailsService;

//        @Autowired
//        private AuthenticationManager authenticationManager;

//        @Autowired
//        private AccessDeniedHandler accessDeniedHandler;
//
//        @Autowired
//        private AuthenticationEntryPoint authenticationEntryPoint;

        @Bean
        public TokenStore tokenStore() {
            return new JdbcTokenStore(dataSource); // table = oauth_access_token
        }

        @Bean
        @Primary
        public DefaultTokenServices tokenServices() {
            DefaultTokenServices tokenServices = new DefaultTokenServices();
            tokenServices.setSupportRefreshToken(true);
//            tokenServices.setAuthenticationManager(authenticationManager);
            tokenServices.setTokenStore(tokenStore());
            return tokenServices;
        }

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints
                    .tokenStore(tokenStore())
//                    .tokenServices(tokenServices())
//                    .authenticationManager(authenticationManager)
                    .userDetailsService(userDetailsService);
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.jdbc(dataSource); // table = oauth_client_details
        }

        @Override
        public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
            security
//                    .allowFormAuthenticationForClients()
//                    .authenticationEntryPoint(authenticationEntryPoint)
                    .checkTokenAccess("isAuthenticated()")
//                    .accessDeniedHandler(accessDeniedHandler)
//                    .and()
//                    .authorizeRequests()
                        ;
        }
    }

    @Configuration
    @EnableResourceServer
//@ComponentScan({"nl.scalda.platenbaas.API.controllers", "nl.scalda.platenbaas.API.dao", "nl.scalda.platenbaas.API.service", "nl.scalda.platenbaas.API.model"})
//@EntityScan({"nl.scalda.platenbaas.API.model"})
    public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

        @Autowired
        private TokenStore tokenStore;

        private CustomLogoutSuccessHandler customLogoutSuccessHandler;

        @Override
        public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
            resources
                    .tokenStore(tokenStore);
        }

        @Override
        public void configure(HttpSecurity http) throws Exception {

            http
                    .authorizeRequests()
                    .antMatchers("/hello/", "/oauth/**", "/oauth2/**").permitAll()
                    .antMatchers("/api/**").authenticated()
                    .and()
                    .exceptionHandling()
                    .and()
                    .logout()
                    .logoutUrl("/api/logout")
                    .logoutSuccessUrl("/api/loggedout")
                    //.logoutSuccessHandler(customLogoutSuccessHandler)
                    ;

        }

//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests()
//                .antMatchers("/oauth/**").permitAll()
//                .antMatchers("/getproductbyeancode").hasRole("USER")
//                .antMatchers("/getallproducts").hasRole("ADMIN")
//                .antMatchers("/users").hasRole("ADMIN")
//                .and()
//                .formLogin().disable();
//
//    }
    }
}
