//package nl.scalda.platenbaas.API.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//import org.springframework.security.oauth2.provider.token.TokenStore;
//
///**
// * @author hunteroooox
// */
//@Configuration
//@EnableResourceServer
////@ComponentScan({"nl.scalda.platenbaas.API.controllers", "nl.scalda.platenbaas.API.dao", "nl.scalda.platenbaas.API.service", "nl.scalda.platenbaas.API.model"})
////@EntityScan({"nl.scalda.platenbaas.API.model"})
//public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
//
//    @Autowired
//    private TokenStore tokenStore;
//
//    @Autowired
//    private CustomLogoutSuccessHandler customLogoutSuccessHandler;
//
//    @Override
//    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
//        resources
//                .tokenStore(tokenStore);
//    }
//
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//
//        http
//                .authorizeRequests()
//                .antMatchers("/hello/", "/oauth/**", "/oauth2/**").permitAll()
//                .antMatchers("/api/**").authenticated()
//                .and()
//                .exceptionHandling()
//                .and()
//                .logout()
//                .logoutUrl("api/logout")
//                .logoutSuccessHandler(customLogoutSuccessHandler);
//
//    }
//
////    @Override
////    public void configure(HttpSecurity http) throws Exception {
////        http
////                .authorizeRequests()
////                .antMatchers("/oauth/**").permitAll()
////                .antMatchers("/getproductbyeancode").hasRole("USER")
////                .antMatchers("/getallproducts").hasRole("ADMIN")
////                .antMatchers("/users").hasRole("ADMIN")
////                .and()
////                .formLogin().disable();
////
////    }
//}
