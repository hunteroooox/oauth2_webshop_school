//package nl.scalda.platenbaas.API.config;
//
//import javax.sql.DataSource;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
///**
// * @author Jeroen
// */
//@Configuration
//@EnableWebSecurity
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//    
//    @Autowired
//    DataSource dataSource;
//    
////    @Autowired
////    private AuthenticationManager authenticationManager;
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http
//                .authorizeRequests().antMatchers("/**").hasRole("USER").and().formLogin();
//                //.formLogin().disable(); // disable form authentication
//                //.anonymous().disable(); // disable anonymous user
//                //.denyAll(); // denying all access
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth
//                .jdbcAuthentication()
//                .withDefaultSchema()
//                .dataSource(dataSource)
//                .getUserDetailsService()
//                .setAuthenticationManager(authenticationManagerBean());
////                .inMemoryAuthentication()
////                .withUser("user")
////                .password("password")
////                .roles("USER")
////                .authorities("ROLE_USER")
////                .and()
////                .withUser("adminr")
////                .password("password")
////                .roles("ADMIN", "USER")
////                .authorities("ROLE_ADMIN");
//    }
//
//    @Override
//    @Bean
//    public AuthenticationManager authenticationManagerBean() throws Exception {
//        // provides the default AuthenticationManager as a Bean
//        return super.authenticationManagerBean();
//    }
//}
