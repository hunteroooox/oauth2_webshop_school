package nl.scalda.platenbaas.API.dao;

import org.springframework.stereotype.Component;

/**
 * @author Jeroen
 */
@Component
public class FactoryDAO {
    
    //Creates singleton object
    private static FactoryDAO instance;

//    //gets product dao
//    public ProductHibernateDAO getProductHibernateDAO() {
//        return ProductHibernateDAO.getInstance();
//    }
//
//    //gets order dao
//    public OrderHibernateDAO getOrderHibernateDAO() {
//        return OrderHibernateDAO.getInstance();
//    }
    
    public static FactoryDAO getInstance() {
        if (instance == null) {
            return new FactoryDAO();
        }
        return instance;
    }
}
