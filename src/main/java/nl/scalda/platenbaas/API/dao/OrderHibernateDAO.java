package nl.scalda.platenbaas.API.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import nl.scalda.platenbaas.API.model.Order;
import nl.scalda.platenbaas.API.model.Order_;
import org.springframework.stereotype.Repository;

/**
 * @author Jeroen
 */
@Repository
public class OrderHibernateDAO implements IBaseDAO<Order>, IOrderHibernateDAO  {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Order> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> criteria = criteriaBuilder.createQuery(Order.class);
        
        Root<Order> root = criteria.from(Order.class);
        criteria.select(root);
        
        try {
            return entityManager.createQuery(criteria).getResultList();

        } catch (Exception e) {
//            TODO: properly catch/throw exception
//            throw e;
            System.out.println("findAll() Orders is misgegaan! Met message: " + e.getMessage());
            return null;
        }
    }

    @Override
    public void saveOrUpdate(Order order) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaUpdate<Order> update = criteriaBuilder.createCriteriaUpdate(Order.class);
        Root<Order> root = update.from(Order.class);

        update.set(Order_.orderID, order.getOrderID());
        update.set(Order_.orderState, order.getOrderState());
        update.set(Order_.orderType, order.getOrderType());
        update.set(Order_.prefix, order.getPrefix());
        update.set(Order_.firstname, order.getFirstname());
        update.set(Order_.surname, order.getSurname());
        update.set(Order_.email, order.getEmail());
        update.set(Order_.address, order.getAddress());
        update.set(Order_.city, order.getCity());
        update.set(Order_.country, order.getCountry());
        update.set(Order_.postalCode, order.getPostalCode());
        update.set(Order_.phonenumber, order.getPhonenumber());
        update.set(Order_.timestamp, order.getTimestamp());
        update.set(Order_.totalPrice, order.getTotalPrice());

        update.where(criteriaBuilder.equal(root.get(Order_.orderID), order.getOrderID()));

        try {
            entityManager.createQuery(update).executeUpdate();
        } catch (Exception e) {
//            TODO: properly catch/throw exception
//            throw e;
            System.out.println("Order is niet geupdate, er is iets fout gegaan! Met message: " + e.getMessage());
        }
        System.out.println("Order met OrderID: " + order.getOrderID() + " is succesvol geupdate!");
    }

    @Override
    public void deleteById(Integer id) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete<Order> delete = criteriaBuilder.createCriteriaDelete(Order.class);

        Root<Order> root = delete.from(Order.class);
        delete.where(criteriaBuilder.equal(root.get(Order_.orderID), id));

        try {
            entityManager.createQuery(delete).executeUpdate();
        } catch (Exception e) {
//            TODO: properly catch/throw exception
//            throw e;
            System.out.println("Order is niet verwijderd, er is iets fout gegaan! Met message: " + e.getMessage());
        }
        System.out.println("Order met OrderID: " + id + " is succesvol verwijderd!");
    }

    @Override
    public Order findOrderById(Integer id) {
        return entityManager.find(Order.class, id);
    }

    @Override
    public Order findProductsByOrderId(Integer id) {
        return entityManager.find(Order.class, id);
    }

}
