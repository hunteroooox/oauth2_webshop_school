package nl.scalda.platenbaas.API.dao;

import java.util.List;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import nl.scalda.platenbaas.API.model.Product;
import nl.scalda.platenbaas.API.model.Product_;
import org.springframework.stereotype.Repository;

/**
 * @author Jeroen
 */
@Repository
public class ProductHibernateDAO implements IBaseDAO<Product>, IProductHibernateDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Product> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteria = criteriaBuilder.createQuery(Product.class);

        Root<Product> root = criteria.from(Product.class);
        criteria.select(root);

        try {
            return entityManager.createQuery(criteria).getResultList();
        } catch (Exception e) {
//            TODO: properly catch/throw exception
//            throw e;
            System.out.println("findAll() Producten is misgegaan! Met message: " + e.getMessage());
            return null;
        }
    }

    @Override
    public void saveOrUpdate(Product product) {
        EntityManager em = entityManager.getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        try {
            Product temp = new Product();
            temp.setEancode(product.getEancode());
            temp.setQuantity(product.getQuantity());
            temp.setPrice(product.getPrice());
            temp.setArtist(product.getArtist());
            temp.setTitle(product.getTitle());
            temp.setUnits(product.getUnits());
            temp.setMedia(product.getMedia());
            temp.setLabel(product.getLabel());
            temp.setPrefix(product.getPrefix());
            temp.setSuffix(product.getSuffix());
            temp.setGenre(product.getGenre());
            temp.setRelease(product.getRelease());

            //product.getEancode(), product.getQuantity(), product.getPrice(), product.getArtist(), product.getTitle(), product.getUnits(), 
            //product.getMedia(), product.getLabel(), product.getPrefix(), product.getSuffix(), product.getGenre(), product.getRelease()
            em.persist(temp);
            em.flush();
            em.getTransaction().commit();

            System.out.println("Product met EANCode: " + product.getEancode() + " en met titel: " + product.getTitle() + " is succesvol geupdate!");
        } catch (EntityExistsException eex) {
            CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

            CriteriaUpdate<Product> update = criteriaBuilder.createCriteriaUpdate(Product.class);
            Root<Product> root = update.from(Product.class);

            update.set(Product_.artist, product.getArtist());
            update.set(Product_.eancode, product.getEancode());
            update.set(Product_.genre, product.getGenre());
            update.set(Product_.label, product.getLabel());
            update.set(Product_.media, product.getMedia());
            update.set(Product_.prefix, product.getPrefix());
            update.set(Product_.price, product.getPrice());
            update.set(Product_.quantity, product.getQuantity());
            update.set(Product_.release, product.getRelease());
            update.set(Product_.suffix, product.getSuffix());
            update.set(Product_.title, product.getTitle());
            update.set(Product_.units, product.getUnits());

            update.where(criteriaBuilder.equal(root.get(Product_.eancode), product.getEancode()));

            try {
                entityManager.createQuery(update).executeUpdate();
            } catch (Exception e) {
//            TODO: properly catch/throw exception
//            throw e;
                System.out.println("Save is mislukt, update is ook miskult");
                System.out.println(e.getStackTrace().toString());
            }
        } catch (Exception e) {
//            TODO: properly catch/throw exception
            System.out.println("error: save product");
//            System.out.println("Product is niet geupdate, er is iets fout gegaan! Met message: " + e.getMessage() + " en stacktrace: ");
            throw e;
        } finally {
            if (!em.getTransaction().isActive() || em.getTransaction().getRollbackOnly()) {
                em.getTransaction().rollback();
            }
            em.close();
        }
    }

    @Override
    public void deleteByEAN(String EANCode) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaDelete<Product> delete = criteriaBuilder.createCriteriaDelete(Product.class);

        Root<Product> root = delete.from(Product.class);
        delete.where(criteriaBuilder.equal(root.get(Product_.eancode), EANCode));

        try {
            entityManager.createQuery(delete).executeUpdate();
        } catch (Exception e) {
//            TODO: properly catch/throw exception
//            throw e;
            //System.out.println("Product is niet verwijderd, er is iets fout gegaan! Met message: " + e.getMessage());
        }
        System.out.println("Product met EANCode: " + EANCode + " is succesvol verwijderd!");
    }

    @Override
    public Product getProductByEAN(String EANCode) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteria = criteriaBuilder.createQuery(Product.class);

        Root<Product> root = criteria.from(Product.class);
        criteria.select(root);
        criteria.where(criteriaBuilder.equal(root.get(Product_.eancode), EANCode));

        Product result;

        try {
            result = entityManager.createQuery(criteria).getSingleResult();
        } catch (Exception e) {
//            TODO: properly catch/throw exception
//            throw e;
            System.out.println("Product kon niet worden gevonden in de database! Met message: " + e.getMessage());
            return null;
        }

        return result;
    }

    @Override
    public boolean checkIfProductExists(Product product) {
        return entityManager.find(Product.class, product.getEancode()) != null;
    }
}
