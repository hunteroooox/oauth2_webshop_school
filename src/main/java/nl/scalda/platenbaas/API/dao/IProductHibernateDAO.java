package nl.scalda.platenbaas.API.dao;

import nl.scalda.platenbaas.API.model.Product;

/**
 * @author Jeroen
 */
public interface IProductHibernateDAO {
    /**
     * @param EANCode - ID of Entity to delete
     */
    void deleteByEAN(String EANCode);
    /**
     * Find all products by EAN Code
     *
     * @param EANCode - EANCode of product you want returned
     * @return A product for provided EAN
     */
    Product getProductByEAN(String EANCode);
    
    /**
     * Check if provided product exists
     *
     * @param product product object
     * @return boolean
     */
    boolean checkIfProductExists(Product product);
}
