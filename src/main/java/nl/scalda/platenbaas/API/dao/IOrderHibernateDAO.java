/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.scalda.platenbaas.API.dao;

import nl.scalda.platenbaas.API.model.Order;

/**
 *
 * @author Jeroen
 */
public interface IOrderHibernateDAO {
    /**
     * @param id - ID of Entity to delete
     */
    void deleteById(Integer id);
    /**
     * Find all orders by ID.
     * 
     * @param id - ID of Entity you want returned
     * @return All Entity where id = {id}
     */
    Order findOrderById(Integer id);

    /**
     * Find all products by order ID.
     *
     * @param id - ID of Order to search product_order with
     * @return All products for provided order id
     */
    Order findProductsByOrderId(Integer id);

//    /**
//     * Create an order for provided customer.
//     *
//     * @param customerId - ID of customer to create order for.
//     * @param order - Order to insert
//     */
//    void createOrder(Integer customerId, Order order);
}
