package nl.scalda.platenbaas.API.dao;

import java.util.List;

/**
 *
 * @author Jeroen
 * @param <Entity>
 */
public interface IBaseDAO<Entity> {
    
    /**
     * @return All Entity's for provided Entity
     */
    List<Entity> findAll();

    /**
     * @param entity - ID of Entity to save/update
     */
    void saveOrUpdate(Entity entity);
}
