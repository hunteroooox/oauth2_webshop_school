package nl.scalda.platenbaas.API.controllers;

import java.util.Date;
import java.util.List;
import nl.scalda.platenbaas.API.model.Product;
import nl.scalda.platenbaas.API.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

/**
 * @author Jeroen
 */
@RestController
public class ProductController {
    
    @Autowired
    private ProductService productDAO;
    
    @RequestMapping(value = "/api/getallproducts", method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getAllProducts() {

        List<Product> p =  productDAO.findAllProducts();

        if (p != null) {
            return new ResponseEntity<>(p, HttpStatus.OK);
            //return new ResponseEntity<>(p, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }
    
    @RequestMapping(value = "/api/getproductbyeancode", method = RequestMethod.GET)
    public ResponseEntity<Product> getProductByEANCode(@RequestParam(value = "eancode") String EANCode) {

        Product p = productDAO.getProductByEAN(EANCode);

        if (p != null) {
            return new ResponseEntity<>(p, HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.CONFLICT);
        }

    }
    
    @RequestMapping(value = "/api/addproduct", method = RequestMethod.POST)
    public ResponseEntity<HttpStatus> saveProduct(WebRequest request) {
        
        Product p = new Product(
                request.getParameter("EANCode"), 
                Integer.parseInt(request.getParameter("Quantity")), 
                Double.parseDouble(request.getParameter("Price")), 
                request.getParameter("Artist"), 
                request.getParameter("Title"), 
                Integer.parseInt(request.getParameter("Units")), 
                request.getParameter("Media"), 
                request.getParameter("Label"), 
                request.getParameter("Prefix").equals("") ? null : request.getParameter("Prefix"), 
                request.getParameter("Suffix").equals("") ? null : request.getParameter("Suffix"), 
                request.getParameter("Genre"), 
                request.getParameter("Release").equals("") ? new Date() : new Date(request.getParameter("Release"))
        );
                
        productDAO.saveProduct(p);

        if (p != null) {
            return new ResponseEntity(HttpStatus.OK);
        } else {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

    }
    
// TODO: DEZE LATEN STAAN!!! NOOIT UIT COMMENTEN!!!!! DAN WERKT DE APPLICATIE NIET MEER!
//    @RequestMapping(method = {RequestMethod.GET, RequestMethod.HEAD}, value = "/login") 
//    public String loginHandler(){
//        return "login";
//    }
}
