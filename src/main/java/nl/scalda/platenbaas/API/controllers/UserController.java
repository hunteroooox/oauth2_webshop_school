package nl.scalda.platenbaas.API.controllers;

import nl.scalda.platenbaas.API.data.User;
import nl.scalda.platenbaas.API.data.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Jeroen
 */
@RestController
public class UserController {

	private final UserRepository userRepository;

	@Autowired
	public UserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@RequestMapping("/api/users")
	public Iterable<User> getUsers() {
		return userRepository.findAll();
	}

}
