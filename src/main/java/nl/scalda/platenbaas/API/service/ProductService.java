package nl.scalda.platenbaas.API.service;

import java.util.List;
import nl.scalda.platenbaas.API.dao.ProductHibernateDAO;
import nl.scalda.platenbaas.API.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Jeroen
 */
@Service
public class ProductService {

    private final ProductHibernateDAO productDAO;

    @Autowired
    public ProductService(ProductHibernateDAO productDAO) {
        this.productDAO = productDAO;
    }

    public List<Product> findAllProducts() {
        return productDAO.findAll();
    }

    public Product getProductByEAN(String EANCode) {
        return productDAO.getProductByEAN(EANCode);
    }

    public void saveProduct(Product product) {
        productDAO.saveOrUpdate(product);
    }

    public void deleteByEAN(String EANCode) {
        productDAO.deleteByEAN(EANCode);
    }

    public boolean checkIfProductExists(Product product) {
        return productDAO.checkIfProductExists(product);
    }
}
