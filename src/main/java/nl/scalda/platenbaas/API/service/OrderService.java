package nl.scalda.platenbaas.API.service;

import java.util.List;
import nl.scalda.platenbaas.API.dao.OrderHibernateDAO;
import nl.scalda.platenbaas.API.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Jeroen
 */
@Service
public class OrderService {
    
private final OrderHibernateDAO orderDAO;

    @Autowired
    public OrderService(OrderHibernateDAO orderDAO) {
        this.orderDAO = orderDAO;
    }

    public List<Order> findAllOrders() {
        return orderDAO.findAll();
    }

    public void saveProduct(Order order) {
        orderDAO.saveOrUpdate(order);
    }

    public void deleteById(Integer id) {
        orderDAO.deleteById(id);
    }

    public Order findOrderById(Integer id) {
        return orderDAO.findOrderById(id);
    }
}
